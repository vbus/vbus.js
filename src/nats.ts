import * as nats from 'ts-nats';
import {Subscription} from 'ts-nats';
import {
    ANONYMOUS_USER,
    fromVbus,
    generatePassword,
    getHostname,
    joinPath,
    mdnsSearch,
    sleep,
    testVbusUrl,
    toVbus
} from "./helpers";
import * as path from "path";
import * as fs from "fs";
import * as bcrypt from "bcrypt";
import {getLogger} from "log4js";
import * as isIp from 'is-ip'
import * as dns from "dns";

const log = getLogger();

// A Nats callback, that take data and path segment
type NatsCallback = (data: any, segments: string[]) => Promise<any>

/**
 * An extended nats client.
 */
export class ExtendedNatsClient {
    private hostname: string = null;
    private remoteHostname: string = null;
    private readonly hubId: string = null;
    private readonly id: string = null;
    private env: IEnv;
    private rootFolder: string;
    private client: nats.Client;

    // Creates a new extended nats client. You must call initialize() before using it.
    constructor(appDomain, appId: string, hubId: string = null) {
        this.id = `${appDomain}.${appId}`;
        this.hubId = hubId;
        this.env = this.readEnvVar();
    }

    async connect() {
        await this.initialize();

        const config = this.readOrGetDefaultConfig();
        const resp = await this.findVbusUrl(config);

        // update the config file with the new url
        config.vbus.url = resp.serverUrl;

        // check if we need to update remote host
        if (resp.newHost !== "") {
            this.remoteHostname = resp.newHost
        }

        this.saveConfigFile(config);
        await this.publishUser(resp.serverUrl, config);
        this.client = await nats.connect({
            url: resp.serverUrl,
            user: config.client.user,
            pass: config.key.private,
            name: config.client.user,
        });
    }

    close() {
        this.client.close()
    }

    private async initialize() {
        this.hostname = await getHostname();

        if (this.hubId) {
            this.remoteHostname = this.hubId;
        } else {
            this.remoteHostname = this.hostname;
        }

        this.rootFolder = this.env.vbusPath;
        // generate a default location is not specified
        if (this.rootFolder === "" || this.rootFolder === undefined || this.rootFolder === null) {
            this.rootFolder = path.join(this.env.home, "vbus")
        }
    }

    private readEnvVar(): IEnv {
        return {
            home: process.env.HOME,
            vbusPath: process.env.VBUS_PATH,
            vbusUrl: process.env.VBUS_URL,
        }
    }

    getRawClient(): nats.Client {
        return this.client;
    }

    getHostname(): string {
        return this.hostname
    }

    getId(): string {
        return this.id
    }

    async findVbusUrl(config: IConfiguration): Promise<FindUrlResp> {
        // find Vbus server - strategy 0: get from argument
        const getFromHubId = async (): Promise<StrategyResp> => {
            return new Promise<StrategyResp>((resolve, reject) => {
                if (isIp(this.remoteHostname)) {
                    resolve({urlsToTest: [`nats://${this.remoteHostname}:21400`], newHost: null})
                } else {
                    dns.lookup(this.remoteHostname, (error, address, family) => {
                        if (error) {
                            reject(error.message);
                        } else {
                            resolve({urlsToTest: [`nats://${address}:21400`], newHost: null})
                        }
                    });
                }
            })
        };

        // find Vbus server - strategy 1: get url from config file
        const getFromConfigFile = async (): Promise<StrategyResp> => {
            return {urlsToTest: [config.vbus.url], newHost: null}
        };

        // find vbus server  - strategy 2: get url from ENV:VBUS_URL
        const getFromEnv = async (): Promise<StrategyResp> => {
            return {urlsToTest: [this.env.vbusUrl], newHost: null}
        };

        // find vbus server  - strategy 3: try default url client://hostname:21400
        const getDefault = async (): Promise<StrategyResp> => {
            return {urlsToTest: [`nats://${this.hostname}.veeamesh.local:21400`], newHost: null}
        };

        // find vbus server  - strategy 4: find it using avahi
        const getFromZeroconf = async (): Promise<StrategyResp> => {
            const resp = await mdnsSearch()
            return {urlsToTest: resp.urls, newHost: resp.newHost}
        }

        const findVbusUrlStrategies = [
            getFromHubId,
            getFromConfigFile,
            getFromEnv,
            getDefault,
            getFromZeroconf,
        ];

        let success = false;
        log.debug("searching for vbus url");
        for (const strategy of findVbusUrlStrategies) {
            log.debug("trying strategy: %s", strategy.name);
            const resp = await strategy();
            for (const url of resp.urlsToTest) {
                if (await testVbusUrl(url)) {
                    log.debug("url found using strategy '%s': %s", strategy.name, url);
                    success = true;
                    return {
                        newHost: resp.newHost,
                        serverUrl: url,
                    }
                } else {
                    log.debug("cannot find a valid url using strategy '%s': %s", strategy.name, url)
                }
            }
        }

        if (!success) {
            throw new Error("no valid url")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Advanced Nats Functions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private getPath(base: string, withHost: boolean, withId: boolean): string {
        let natsPath = base;
        if (withHost) {
            natsPath = joinPath(this.hostname, base)
        }
        if (withId) {
            natsPath = joinPath(this.id, natsPath)
        }
        return natsPath
    }

    async request(base: string, data: any, withHost: boolean = true, withId: boolean = true, timeoutMs: number = 1000): Promise<any> {
        const natsPath = this.getPath(base, withHost, withId);
        const msg = await this.client.request(natsPath, timeoutMs, toVbus(data));
        return fromVbus(msg.data)
    }

    async publish(base: string, data: any, withHost: boolean = true, withId: boolean = true) {
        const natsPath = this.getPath(base, withHost, withId);
        this.client.publish(natsPath, toVbus(data));
        await this.client.flush();
    }

    // Utility method that automatically parse subject wildcard and chevron to arguments.
    // If a value is returned, it is published on the reply subject.
    // Example:
    async subscribe(base: string, cb: NatsCallback, withHost: boolean = true, withId: boolean = true, timeoutMs: number = 500): Promise<Subscription> {
        const natsPath = this.getPath(base, withHost, withId);
        // create a regex that capture wildcard and chevron in path
        let regex = natsPath.replace(".", `\.`,); // escape dot
        regex = regex.replace("*", `([^.]+)`); // capture wildcard
        regex = regex.replace(">", `(.+)`);    // capture chevron

        return await this.client.subscribe(natsPath, async (err, msg) => {
            if (err) {
                log.error("error in subscribe: %s", err.message);
                return
            }
            const groups = msg.subject.match(regex);

            try {
                const data = fromVbus(msg.data);
                const resp = await cb(data, groups.slice(1)); // skip first match (full match)
                // if there is a reply subject, use it to send response
                if (msg.reply) {
                    this.client.publish(msg.reply, toVbus(resp))
                }
            } catch (e) {
                log.error(e);
                return
            }
        })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Authentication
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private async publishUser(url: string, config: IConfiguration) {
        const conn = await nats.connect({url, user: ANONYMOUS_USER, pass: ANONYMOUS_USER});
        const data = toVbus(config.client);
        conn.publish(`system.authorization.${this.remoteHostname}.add`, data);
        await sleep(2000)
    }

    async askPermission(permission: string): Promise<boolean> {
        const config = this.readOrGetDefaultConfig();
        config.client.permissions.subscribe.push(permission);
        config.client.permissions.publish.push(permission);
        const natsPath = `system.authorization.${this.remoteHostname}.${this.id}.${this.hostname}.permissions.set`;
        const resp = await this.request(natsPath, config.client.permissions, false, false, 5000);
        this.saveConfigFile(config);
        return resp as boolean
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Configuration
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    readOrGetDefaultConfig(): IConfiguration {
        if (!fs.existsSync(this.rootFolder)) {
            fs.mkdirSync(this.rootFolder);
        }

        log.debug("check if we already have a Vbus config file in %s", this.rootFolder);

        const configFile = path.join(this.rootFolder, this.id) + ".conf";
        if (fs.existsSync(configFile)) {
            log.debug("load existing configuration file for %s", this.id);
            const rawData = fs.readFileSync(configFile, 'utf8');
            return JSON.parse(rawData) as IConfiguration
        } else {
            log.debug("create new configuration file for %s", this.id);
            return this.getDefaultConfig()
        }
    }

    getDefaultConfig(): IConfiguration {
        log.debug("create new configuration file for %s", this.id);

        const password = generatePassword();
        const salt = bcrypt.genSaltSync(11, 'a');
        const publicKey = bcrypt.hashSync(password, salt);

        return {
            client: {
                user: `${this.id}.${this.hostname}`,
                password: publicKey,
                permissions: {
                    subscribe: [
                        this.id,
                        `${this.id}.>`
                    ],
                    publish: [
                        this.id,
                        `${this.id}.>`
                    ]
                }
            },
            key: {
                private: password
            },
            vbus: {
                url: ""
            }
        }
    }

    saveConfigFile(config: IConfiguration) {
        const data = JSON.stringify(config);
        fs.writeFileSync(path.join(this.rootFolder, this.id) + ".conf", data);
    }
}

interface Function {
    name: string;
}

interface FindUrlResp {
    serverUrl: string
    newHost: string
}

interface StrategyResp {
    urlsToTest: string[]
    newHost: string | null
}

interface IEnv {
    home: string
    vbusPath: string
    vbusUrl: string
}

interface IConfiguration {
    client: ClientConfig
    key: KeyConfig
    vbus: VbusConfig
}

interface ClientConfig {
    user: string
    password: string
    permissions: PermConfig
}

interface PermConfig {
    subscribe: string[]
    publish: string[]
}

interface KeyConfig {
    private: string
}

interface VbusConfig {
    url: string
}

