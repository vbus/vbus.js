import * as os from 'os';
import * as dbus from 'dbus';
import * as mdns from 'mdns';
import {getLogger} from "log4js"
import {generate} from 'generate-password'
import * as nats from 'ts-nats';
import {url} from "inspector";

const log = getLogger();

export const NOTIF_ADDED = "add";
export const NOTIF_REMOVED = "del";
export const NOTIF_GET = "get";
export const NOTIF_VALUE_GET = "value.get";
export const NOTIF_SETTED = "set";
export const NOTIF_VALUE_SETTED = "value.set";
export const ANONYMOUS_USER = "anonymous";


export function toVbus(obj: any) {
    if (obj === null || obj === undefined) {
        return ""
    }
    return JSON.stringify(obj)
}

export function fromVbus(data: string): any {
    if (data === "" || data === null || data === undefined)
        return null;
    return JSON.parse(data)
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function hasKey(obj: any, key: string): boolean {
    return (typeof obj === 'object' && key in obj)
}

export function arrayEqual(array1, array2: any[]): boolean {
    return array1.length === array2.length && array1.every((element, index) => {
        return element === array2[index];
    });
}

// join path segments to a path (skip empty and null strings)
export function joinPath(...segments: string[]): string {
    return segments.filter(Boolean).join(".")
}

export function isWildcardPath(...parts: string[]): boolean {
    return parts.indexOf("*") > -1
}

export function getPathInObj(o: object, ...segments: string[]): any {
    let root = o;
    for (const segment of segments) {
        if (hasKey(root, segment)) { // element exists
            const v = root[segment];
            if (typeof v === "object") {
                root = v
            } else {
                return null
            }
        } else {
            return null
        }
    }
    return root
}

let dbusConn = null;

// Try to get Hub hostname and fallback on os.hostname in case of error.
export function getHostname(): Promise<string> {
    return new Promise<string>((resolve, rejects) => {
        const hostname: string = os.hostname();

        try {
            if (dbusConn === null) // can only getdbus once
                dbusConn = dbus.getBus("system");
            dbusConn.getInterface("io.veea.VeeaHub.Info",
                "/io/veea/VeeaHub/Info",
                "io.veea.VeeaHub.Info.Hostname", (err, iface) => {
                    if (err) {
                        log.warn("failed to get dbus interface:: %s", err);
                        resolve(hostname)
                    } else {
                        iface.Hostname((e, res) => {
                            if (e) {
                                log.warn("failed to get hostname on dbus:: %s", e);
                                resolve(hostname)
                            } else {
                                resolve(res)
                            }
                        })
                    }
                })
        } catch (e) {
            log.warn("cannot connect to dbus: %s", e);
            resolve(hostname)
        }
    });
}

export function generatePassword(): string {
    return generate({length: 22, numbers: true});
}

export interface MdnsSearchResult {
    urls: string[]
    newHost: string | null
}

export function mdnsSearch(): Promise<MdnsSearchResult> {
    return new Promise<MdnsSearchResult>((resolve, reject) => {
        log.debug("find vbus on network");
        const browser = mdns.createBrowser(mdns.tcp('nats'));

        browser.on('serviceUp', (service) => {
            log.debug("service up: ", service);
            if ("vBus" === service.name) {
                log.debug("vbus found");

                const urls = []
                let hostname = null
                // first url to test
                if ('host' in service.txtRecord) {
                    urls.push(`nats://${service.txtRecord.host}:${service.port.toString()}`)
                    if ('hostname' in service.txtRecord) {
                        hostname = service.txtRecord.hostname
                    }
                }

                // second one
                urls.push(`nats://${service.addresses[0]}:${service.port.toString()}`)
                resolve({newHost: hostname, urls})
            }
        });

        browser.start();

        // Set up the timeout
        setTimeout(() => {
            reject('timeout');
        }, 5000);
    });
}

export async function testVbusUrl(urlToTest: string): Promise<boolean> {
    if (urlToTest === "" || urlToTest === null || urlToTest === undefined) {
        return false
    }

    try {
        const conn = await nats.connect({'url': urlToTest, 'user': ANONYMOUS_USER, 'pass': ANONYMOUS_USER});
        conn.close();
        return true
    } catch (e) {
        return false
    }
}
