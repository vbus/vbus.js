import {arrayEqual, hasKey} from "./helpers";
import {getLogger} from "log4js";
import {JSONSchema7} from 'json-schema'
import {userInfo} from "os";

const log = getLogger();

export abstract class Definition {
    // Search for a path in this definition.
    // It can returns a iDefinition or none if not found.
    searchPath(parts: string[]): Definition {
        if (parts.length <= 0) {
            return this
        }
        return null
    }

    // Tells how to handle a set request from Vbus.
    abstract handleSet(data: any, parts: string[]): any;

    // Tells how to handle a set request from Vbus.
    handleGet(data: any, parts: string[]): any {
        return this.toRepr()
    }

    // Get the Vbus representation.
    abstract toRepr(): object;
}

// Tells if a raw node is an attribute.
export function isAttribute(node: any): boolean {
    return hasKey(node, "schema")
}

// Tells if a raw node is a method.
export function isMethod(node: any): boolean {
    return hasKey(node, "params") && hasKey(node, "returns")
}

// Tells if a raw node is a node.
export function isNode(node: any): boolean {
    return !isAttribute(node) && !isMethod(node) && typeof node === "object"
}

export type SetCallback = (data: any, segment: string[]) => void;
export type GetCallback = (data: any, segment: string[]) => void;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Error Definition
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export class ErrorDefinition extends Definition {
    private readonly code: number;
    private readonly message: string;
    private readonly detail: string;

    constructor(code: number, message: string, detail: string = "") {
        super();
        this.code = code;
        this.message = message;
        this.detail = detail;
    }

    static newPathNotFoundErrorWithDetail(path: string): ErrorDefinition {
        return new ErrorDefinition(404, "not found", path)
    }

    static newInternalError(err: Error): ErrorDefinition {
        return new ErrorDefinition(500, "internal server error", err.message)
    }

    async handleSet(data: any, parts: string[]): Promise<any> {
        log.trace("not implemented");
        return null
    }

    toRepr(): object {
        if (this.detail === "") {
            return {
                code: this.code,
                message: this.message,
            }
        } else {
            return {
                code: this.code,
                message: this.message,
                detail: this.detail,
            }
        }
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Method Definition
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export type MethodDefCallback = (...args: any[]) => Promise<any>;

// A Method definition.
// It holds a user callback.
export class MethodDef extends Definition {
    private readonly method: MethodDefCallback = null;
    private readonly name: string;
    private readonly paramsSchema: JSONSchema7 = null;
    private readonly returnsSchema: JSONSchema7 = null;

    // Creates a new method def with the provided json schema.
    constructor(method: MethodDefCallback, paramsSchema: JSONSchema7, returnsSchema: JSONSchema7) {
        super();
        this.method = method;
        this.name = method.name;
        this.paramsSchema = paramsSchema;
        this.returnsSchema = returnsSchema;
        this.inspectMethod()
    }

    private inspectMethod() {
        if (this.method.length <= 0) {
            throw new Error(`method ${this.name} must have at least one parameter: path: string[]`)
        }
    }

    async handleSet(data: any, parts: string[]): Promise<any> {
        if (Array.isArray(data)) {
            return await this.method(...data, parts);
        } else {
            // consider no args
            return this.method(parts)
        }
    }

    toRepr(): object {
        return {
            params: {
                schema: this.paramsSchema,
            },
            returns: {
                schema: this.returnsSchema,
            },
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Attribute iDefinition
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export class AttributeDef extends Definition {
    private readonly uuid: string;
    public value: any;
    private readonly schema: any;
    private readonly onSet: SetCallback = null;
    private readonly onGet: GetCallback = null;

    constructor(uuid: string, value: any, schema: JSONSchema7, onSet: SetCallback = null, onGet: GetCallback = null) {
        super();
        this.uuid = uuid;
        this.value = value;
        this.schema = schema;
        this.onSet = onSet;
        this.onGet = onGet;
    }

    searchPath(parts: string[]): Definition {
        if (parts.length <= 0) {
            return this
        } else if (arrayEqual(parts, ["value"])) {
            return this
        }
        return null
    }

    async handleSet(data: any, parts: string[]): Promise<any> {
        if (this.onSet) {
            return this.onSet(data, parts)
        }
        log.debug("no set handler attached to %s", this.uuid);
        return null
    }

    handleGet(data: any, parts: string[]): any {
        if (parts[parts.length - 1] === "value") { // request on value
            if (this.onGet) {
                return this.onGet(data, parts)
            } else {
                log.debug("no get handler attached to %s, returning cache", this.uuid)
                return this.value
            }
        } else { // request on definition
            return this.toRepr()
        }
    }

    toRepr(): object {
        if (this.value === null) {
            return {
                schema: this.schema,
            }
        } else {
            return {
                schema: this.schema,
                value: this.value,
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Node iDefinition
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


export type RawNode = object
type NodeStruct = { [uuid: string]: Definition }

// A node definition.
export class NodeDef extends Definition {
    private readonly structure: NodeStruct = null;
    private readonly onSet: SetCallback = null;

    constructor(rawNode: RawNode, onSet: SetCallback = null) {
        super();
        this.structure = this.initializeStructure(rawNode);
        this.onSet = onSet;
    }

    private initializeStructure(rawNode: RawNode): NodeStruct {
        const structure: NodeStruct = {};

        for (const k in rawNode) {
            if (rawNode.hasOwnProperty(k)) {
                const val = rawNode[k];
                if (val instanceof Definition) { // already a def
                    structure[k] = val
                } else if (typeof val === "object") { // a rawnode
                    structure[k] = new NodeDef(val)
                } else { // Raw attribute
                    throw new Error("unknown type in node definition: " + val)
                }
            }
        }
        return structure
    }

    searchPath(parts: string[]): Definition {
        if (parts.length <= 0) {
            return this
        } else if (parts[0] in this.structure) { // recurse
            return this.structure[parts[0]].searchPath(parts.slice(1))
        }
        return null
    }

    handleSet(data: any, parts: string[]): any {
        if (this.onSet) {
            return this.onSet(data, parts)
        }
        log.debug("no onSet handler");
        return null
    }

    toRepr(): object {
        const repr = {};

        // tslint:disable-next-line:forin
        for (const k in this.structure) {
            const v = this.structure[k];
            repr[k] = v.toRepr()
        }

        return repr
    }

    addChild(uuid: string, definition: Definition) {
        this.structure[uuid] = definition;
    }

    removeChild(uuid: string): Definition {
        if (uuid in this.structure) {
            const val = this.structure[uuid];
            delete this.structure[uuid];
            return val
        }
        return null
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Aliases
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export type N = NodeDef
export type A = AttributeDef
export type M = MethodDef




