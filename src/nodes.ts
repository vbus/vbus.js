import {
    AttributeDef,
    Definition,
    ErrorDefinition,
    GetCallback,
    MethodDef,
    MethodDefCallback,
    NodeDef,
    RawNode,
    SetCallback
} from "./definitions";
import {ExtendedNatsClient} from "./nats";
import {
    fromVbus,
    joinPath,
    NOTIF_ADDED,
    NOTIF_GET,
    NOTIF_REMOVED,
    NOTIF_SETTED,
    NOTIF_VALUE_SETTED,
    sleep,
    toVbus
} from "./helpers";
import * as fs from "fs"
import {JSONSchema7} from "json-schema";
import {getLogger} from "log4js";
import {AttributeProxy, MethodProxy, NodeProxy, UnknownProxy} from "./proxies";
import {Subscription} from "ts-nats";
import * as dns from "dns";
import {promisify} from "util";
import * as pathLib from "path";


const EXPOSE_URIS_KEYWORD = "uris";
const RESERVED_KEYWORDS = [EXPOSE_URIS_KEYWORD];
const log = getLogger();

export abstract class Element {
    protected readonly client: ExtendedNatsClient = null;
    protected readonly uuid: string;
    protected readonly definition: Definition = null;
    protected readonly parent: Element = null;

    protected constructor(client: ExtendedNatsClient, uuid: string, definition: Definition, parent: Element) {
        this.client = client;
        this.uuid = uuid;
        this.definition = definition;
        this.parent = parent;
    }

    getUuid(): string {
        return this.uuid;
    }

    getDefinition(): Definition {
        return this.definition;
    }

    getPath(): string {
        if (this.parent) {
            return joinPath(this.parent.getPath(), this.uuid)
        } else {
            return this.uuid
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Node
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * A Vbus connected node.
 * It contains a node definition and send update over Vbus.
 */
export class Node extends Element {
    /**
     * Creates a new Node.
     * @param client {ExtendedNatsClient} nats client
     * @param uuid {string} unique identifier
     * @param definition {NodeDef} definition
     * @param parent {Element} parent element
     */
    constructor(client: ExtendedNatsClient, uuid: string, definition: NodeDef, parent: Element) {
        super(client, uuid, definition, parent);
    }

    /**
     * Add a child node and notify Vbus.
     * @async
     * @param uuid {string} uuid
     * @param rawNode {RawNode} node definition
     * @param onSet {SetCallback} on set event callback
     * @return {Promise<Node>} newly created node
     * @example
     * client.addNode("foo", {
     *     bar: {
     *         baz: {
     *             name: new vbus.AttributeDef("name", "eliott", {type: "string"}),
     *             scan: new vbus.MethodDef((args, path) => {
     *                 return new Promise(((resolve, reject) => {
     *                     console.log(args);
     *                     console.log(path);
     *                     resolve(42)
     *                 }))
     *             }, {}, {})
     *         }
     *     }
     * });
     */
    async addNode(uuid: string, rawNode: RawNode, onSet: SetCallback = null): Promise<Node> {
        const def = new NodeDef(rawNode, onSet);             // create the definition
        const node = new Node(this.client, uuid, def, this); // create the connected node
        (this.definition as NodeDef).addChild(uuid, def);    // add it

        // send the node definition on Vbus
        const packet = {[uuid]: def.toRepr()};
        await this.client.publish(joinPath(this.getPath(), NOTIF_ADDED), packet);
        return node
    }

    /**
     * Add a child attribute and notify Vbus.
     * @async
     * @param uuid {string} uuid
     * @param value initial value
     * @param schema {JSONSchema7} A json-schema to describe value type
     * @param onSet {SetCallback} on set event callback
     * @param onGet {GetCallback} on get event callback
     * @return {Promise<Attribute>} newly created attribute
     */
    async addAttribute(uuid: string, value: any, schema: JSONSchema7, onSet: SetCallback = null, onGet: GetCallback = null): Promise<Attribute> {
        if (RESERVED_KEYWORDS.indexOf(uuid) >= 0) {
            return Promise.reject(new Error(`${uuid} is a reserved keyword`))
        }

        const def = new AttributeDef(uuid, value, schema, onSet, onGet); // create the definition
        const node = new Attribute(this.client, uuid, def, this);        // create the connected node
        (this.definition as NodeDef).addChild(uuid, def);                // add it

        // send the node definition on Vbus
        const packet = {[uuid]: def.toRepr()};
        await this.client.publish(joinPath(this.getPath(), NOTIF_ADDED), packet);
        return node
    }

    /**
     * Add a child attribute and notify Vbus.
     * @async
     * @param uuid {string} uuid
     * @param method {MethodDefCallback} the function to be called
     * @param paramsSchema {JSONSchema7} A json schema to describe params
     * @param returnsSchema {JSONSchema7} A json schema to describe returns
     * @return {Promise<Method>}
     */
    async addMethod(uuid: string, method: MethodDefCallback, paramsSchema: JSONSchema7, returnsSchema: JSONSchema7): Promise<Method> {
        if (RESERVED_KEYWORDS.indexOf(uuid) >= 0) {
            return Promise.reject(new Error(`${uuid} is a reserved keyword`))
        }

        const def = new MethodDef(method, paramsSchema, returnsSchema); // create the definition
        const node = new Method(this.client, uuid, def, this);        // create the connected node
        (this.definition as NodeDef).addChild(uuid, def);                // add it

        // send the node definition on Vbus
        const packet = {[uuid]: def.toRepr()};
        await this.client.publish(joinPath(this.getPath(), NOTIF_ADDED), packet);
        return node
    }

    getAttribute(...path: string[]): Attribute {
        const def = this.definition.searchPath(path);
        if (!def) {
            return null
        }

        // test that the definition is an attribute def
        if (def instanceof AttributeDef) {
            return new Attribute(this.client, joinPath(...path), def, this)
        } else {
            log.warn("trying to retrieve a non attribute");
            return null
        }
    }

    async removeElement(uuid: string) {
        const def = (this.definition as NodeDef).removeChild(uuid);
        if (def) {
            const packet = {uuid: def.toRepr()};
            await this.client.publish(joinPath(this.getPath(), NOTIF_REMOVED), packet);
        }
    }

    toString(): string {
        return JSON.stringify(this.definition.toRepr(), null, 2);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Attribute
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * A VBus connected attribute.
 */
export class Attribute extends Element {
    /**
     * Creates a new Attribute.
     * @param client {ExtendedNatsClient} nats client
     * @param uuid {string} unique identifier
     * @param definition {AttributeDef} definition
     * @param parent {Element} parent element
     */
    constructor(client: ExtendedNatsClient, uuid: string, definition: AttributeDef, parent: Element) {
        super(client, uuid, definition, parent);
    }

    /**
     * Set current value and notify vBus.
     * @async
     * @param value attribute value
     */
    async setValue(value: any) {
        (this.definition as AttributeDef).value = value;
        await this.client.publish(joinPath(this.getPath(), NOTIF_VALUE_SETTED), value)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Method
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * A VBus connected method.
 */
export class Method extends Element {
    /**
     * Creates a new Method.
     * @param client {ExtendedNatsClient} nats client
     * @param uuid {string} unique identifier
     * @param definition {MethodDef} definition
     * @param parent {Element} parent element
     */
    constructor(client: ExtendedNatsClient, uuid: string, definition: MethodDef, parent: Element) {
        super(client, uuid, definition, parent);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Node Manager
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Module status
 */
export interface ModuleStatus {
    /**
     * heap size in bytes
     */
    heapSize: number
}

/**
 * Module information
 */
export interface ModuleInfo {
    /**
     * Application id
     */
    id: string

    /**
     * Hostname
     */
    hostname: string

    /**
     * Client, example: "python", "golang"
     */
    client: string

    /**
     * Has static file exposed ?
     */
    hasStaticFiles: boolean

    /**
     * Status
     */
    status: ModuleStatus
}

/**
 * Node manager options.
 */
export interface NodeManagerOptions {
    /**
     * Hub serial number when trying to connect to a remote hub
     */
    hubId?: string

    /**
     * Static path to be served by vBus http server.
     */
    static_path?: string
}

/**
 * This is the VBus nodes manager.
 * It manage local nodes lifecycle and allow to retrieve remote nodes.
 * The node manager is also the root node of your app.
 *
 * Note: The client inherits from the NodeManager
 * @extends Node
 */
export class NodeManager extends Node {
    private subs: Subscription[] = [];
    private urisNode: Node = null;
    private options: NodeManagerOptions;

    /**
     * Creates a new NodeManager.
     * @param client {ExtendedNatsClient} nats client
     * @param options {NodeManagerOptions} options
     */
    constructor(client: ExtendedNatsClient, options: NodeManagerOptions = null) {
        super(client, "", new NodeDef({}), null);
        this.options = options;
    }

    async initialize() {
        // subscribe to root path
        const sub1 = await this.client.subscribe("", async (data, segments) => {
            // get all nodes
            return {[this.client.getHostname()]: this.definition.toRepr()}
        }, false);
        this.subs.push(sub1);

        // subscribe to all
        const sub2 = await this.client.subscribe(">", async (data, segments) => {
            // get a specific path
            const parts = segments[0].split("."); // split the first segment (">") to string list.
            if (parts.length < 1) {
                return null // invalid path, missing event ("add", "del"...)
            }

            const event = parts.pop();
            return await this.handleEvent(data, event, ...parts)
        });
        this.subs.push(sub2);

        // subscribe to info path
        const sub3 = await this.client.subscribe("info", async (data, segments) => {
            return this.getModuleInfo()
        }, false, false)
        this.subs.push(sub3)

        // handle static file server
        if (this.options.static_path) {
            await this.addMethod("static", async (method: string, uri: string, parts: string[]) => {
                log.debug("static: received %s on %s", method, uri)
                let filePath = pathLib.join(this.options.static_path, uri)
                if (!fs.existsSync(filePath)) {
                    filePath = pathLib.join(this.options.static_path, "index.html") // assume SPA
                }

                const data = fs.readFileSync(filePath)
                const buff = Buffer.from(data);
                return buff.toString('base64')
            }, {
                type: "array",
                items: [
                    {
                        type: "string"
                    },
                    {
                        type: "string"
                    }
                ]
            }, {
                type: "string"
            })
        }
    }

    private getModuleInfo(): ModuleInfo {
        const heapSize = process.memoryUsage().heapTotal;

        return {
            client: "js",
            hasStaticFiles: !!this.options.static_path,
            hostname: this.client.getHostname(),
            id: this.client.getId(),
            status: {
                heapSize
            }
        }
    }

    private async handleEvent(data: any, event: string, ...parts: string[]): Promise<any> {
        const nodeDef = this.definition.searchPath(parts);
        if (nodeDef) { // found

            try {
                let resp: any = null;

                switch (event) {
                    case NOTIF_GET:
                        resp = nodeDef.handleGet(data, parts);
                        break;
                    case NOTIF_SETTED:
                        resp = await nodeDef.handleSet(data, parts);
                        break;
                    default:
                        resp = null;
                }

                return resp
            } catch (e) { // internal error
                log.warn("internal error while handling %s (%s)", joinPath(...parts), e.message);
                return ErrorDefinition.newInternalError(e).toRepr();
            }
        } else { // path not found
            log.warn("path not found: %s", joinPath(...parts));
            return ErrorDefinition.newPathNotFoundErrorWithDetail(joinPath(...parts)).toRepr()
        }
    }

    /**
     * Discover a remote vBus tree (A Vbus tree is composed of Vbus elements).
     * @async
     * @param path path to discover
     * @param timeoutMs timeout in ms
     * @return {Promise<UnknownProxy>} An unknown element proxy
     * @example
     * const elem = await client.discover("system.zigbee", 1000);
     * if (elem.isNode()) {
     *     traverseNode(elem.asNode(), 0)
     * }
     */
    async discover(path: string, timeoutMs: number): Promise<UnknownProxy> {
        let resp: object;

        const rawClient = this.client.getRawClient();
        const inbox = rawClient.createInbox();

        // subscribe to created inbox
        const sub = await rawClient.subscribe(inbox, (err, msg) => {
            if (err) {
                log.warn("error while discovering: %s", err.message)
            } else {
                const data = fromVbus(msg.data);
                if (typeof data !== "object") {
                    log.warn("received data is not a json object")
                } else {
                    resp = Object.assign({}, resp, data) // merge dict
                }
            }
        });

        // publish and wait
        rawClient.publish(path, toVbus(null), inbox);
        await sleep(timeoutMs);

        try {
            sub.unsubscribe();
        } catch (e) {
            log.warn("cannot unsubscribe")
        }
        return new UnknownProxy(this.client, path, resp)
    }

    /**
     * Discover running Vbus modules.
     * @param timeoutMs
     * @return {Promise<ModuleInfo[]>}
     */
    async discoverModules(timeoutMs: number): Promise<ModuleInfo[]> {
        const resp: ModuleInfo[] = [];

        const rawClient = this.client.getRawClient();
        const inbox = rawClient.createInbox();

        // subscribe to created inbox
        const sub = await rawClient.subscribe(inbox, (err, msg) => {
            if (err) {
                log.warn("error while discovering modules: %s", err.message)
            } else {
                const data = fromVbus(msg.data);
                if (typeof data !== "object") {
                    log.warn("received data is not a json object")
                } else {
                    resp.push(data as ModuleInfo)
                }
            }
        });

        // publish and wait
        rawClient.publish("info", toVbus(null), inbox);
        await sleep(timeoutMs);

        try {
            sub.unsubscribe();
        } catch (e) {
            log.warn("cannot unsubscribe")
        }
        return resp
    }

    /**
     * Retrieve a remote node proxy.
     * @async
     * @param parts path segments
     * @return {Promise<NodeProxy>} node proxy
     * @example
     * const remoteNode = await client.getRemoteNode("system", "zigbee", "host", "path", "to", "node")
     */
    async getRemoteNode(...parts: string[]): Promise<NodeProxy> {
        return await new NodeProxy(this.client, "", {}).getNode(...parts)
    }

    /**
     * Retrieve a remote attribute proxy.
     * @async
     * @param parts path segments
     * @return {Promise<AttributeProxy>} attr proxy
     * @example
     * const attr = await client.getRemoteAttr("system", "zigbee", "host", "path", "to", "attr")
     */
    async getRemoteAttr(...parts: string[]): Promise<AttributeProxy> {
        return await new NodeProxy(this.client, "", {}).getAttribute(...parts)
    }

    /**
     * Retrieve a remote method proxy.
     * @async
     * @param parts path segments
     * @return {Promise<MethodProxy>} method proxy
     * @example
     * const meth = await client.getRemoteMethod("system", "zigbee", "host", "path", "to", "meth")
     */
    async getRemoteMethod(...parts: string[]): Promise<MethodProxy> {
        return await new NodeProxy(this.client, "", {}).getMethod(...parts)
    }

    /**
     * Expose a service identified with an uri on Vbus.
     * @param name The service name
     * @param protocol The protocol name (http, https, ws, ...)
     * @param port The port number
     * @param path The service path
     */
    async expose(name: string, protocol: string, port: number, path: string) {
        // resolve current hostname to get ip address
        const dnsLookup = promisify(dns.lookup); // transform to promise like func
        const ipAddress = await dnsLookup(this.client.getHostname());
        const uri = `${protocol}://${ipAddress.address}:${port}/${path}`;

        if (this.urisNode === null) {
            this.urisNode = await this.addNode(EXPOSE_URIS_KEYWORD, {});
        }

        await this.urisNode.addAttribute(name, uri, {type: "string"})
    }
}






