const vbus = require("../dist/");


// set vbus library log level
vbus.setLoggerLevel("debug");

// top level await (vbus library is async)
(async () => {
    try {
        const client = new vbus.Client("system", "testjs", {static_path: "./"});
        await client.connect();
    } catch (e) {
        console.log(e)
    }
})();

