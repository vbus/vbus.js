const vbus = require("../dist/");

// set vbus library log level
vbus.setLoggerLevel("debug");

// top level await (vbus library is async)
(async () => {
    try {
        const client = new vbus.Client("system", "testjs");
        await client.connect();
        console.log("connected");

        const modules = await client.discoverModules(1000);
        modules.forEach(m => console.log(m))
    } catch (e) {
        console.log(e)
    }
})();

