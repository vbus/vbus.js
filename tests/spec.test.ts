/**
 * Test against the vbus spec.
 */
import * as assert from "assert"
import * as player from "nats-scenario-player"
import * as path from "path";
import * as os from "os";
import * as fs from "fs";
import * as vbus from "../src";

vbus.setLoggerLevel("debug")

describe('VBus library',
    function () {
        this.timeout(10000);

        it('should ask permission', async function () {
            const p = setupTest("ask_permission.json");
            const client = await assertNewClient();

            // test success
            let resp = await client.askPermission("should.be.true");
            assert.equal(resp, true);

            // test failure
            resp = await client.askPermission("should.be.false");
            assert.equal(resp, false);

            client.close();
            await assertPlayerSuccess(p);
        });

        it('should add attribute', async function () {
            const p = setupTest("add_attribute.json");
            const client = await assertNewClient();
            const attr = await client.addAttribute("name", "HEIMAN", {type: "string"});
            client.close();
            await assertPlayerSuccess(p);
        });

        it('should set attribute', async function () {
            const p = setupTest("set_attribute.json");
            const client = await assertNewClient();
            const attr = await client.addAttribute("name", "HEIMAN", {type: "string"});
            await attr.setValue("hello world");

            client.close();
            await assertPlayerSuccess(p);
        });

        it('should get remote attribute', async function () {
            const p = setupTest("remote_attribute_get.json");
            const client = await assertNewClient();

            const remoteAttr = await client.getRemoteAttr("test", "remote", client.getHostname(), "name");
            assert.notEqual(null, remoteAttr);
            const val = await remoteAttr.readValue();
            assert.equal("HEIMAN", val);

            client.close();
            await assertPlayerSuccess(p);
        });

        it('should add method', async function () {
            const p = setupTest("add_method.json");
            const client = await assertNewClient();

            const echo = async (msg: string, parts: string[]): Promise<string> => {
                return msg
            };

            const meth = await client.addMethod("echo", echo, {
                type: "array",
                items: [{type: "string"}]
            }, {type: "string"});
            assert.notEqual(null, meth);

            client.close();
            await assertPlayerSuccess(p);
        });

        it('should call remote method', async function () {
            const p = setupTest("call_remote_method.json");
            const client = await assertNewClient();

            const echo = await client.getRemoteMethod("test", "remote", client.getHostname(), "echo");
            assert.notEqual(null, echo);
            const resp = echo.call("hello world");
            assert.equal("hello world", resp),

            client.close();
            await assertPlayerSuccess(p);
        })
    }
);

after('Exit mocha gracefully after finishing all tests execution', function () {
    // Exit node process
    process.exit();
});

function setupTest(scenario: string): player.Player {
    const confFile = path.join(process.env.HOME, "vbus/test.vbusjs.conf");
    if (fs.existsSync(confFile)) {
        fs.unlinkSync(confFile)
    }

    const p = new player.Player("test.vbusjs");
    p.playScenario(path.join(__dirname, `scenarios/${scenario}`));
    return p;
}

// Create and connect a new Vbus client.
async function assertNewClient(): Promise<vbus.Client> {
    const client = new vbus.Client("test", "vbusjs");
    await client.connect();
    return client
}

async function assertPlayerSuccess(p: player.Player) {
    await p.waitDone();
    assert.equal(true, p.isSuccess());
    p.stop();
}
