import {getLogger} from "log4js";

export {Client} from "./client"
export {
    NodeDef,
    ErrorDefinition,
    MethodDef,
    AttributeDef,
    Definition,
    RawNode,
    SetCallback,
    GetCallback,
    MethodDefCallback,
    A, M, N
} from "./definitions";
export {ExtendedNatsClient} from "./nats"
export {Element, Node, Method, Attribute, NodeManager} from "./nodes"
export {NodeProxy, UnknownProxy, AttributeProxy, MethodProxy} from "./proxies"

const log = getLogger();

export function setLoggerLevel(level: string) {
    log.level = level;
}