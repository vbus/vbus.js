// The Vbus client. Library entry point.
import {NodeManager, NodeManagerOptions} from "./nodes";
import {ExtendedNatsClient} from "./nats";


/**
 * A Vbus client that allows you to discover other bridges.
 * Before using it, you must connect it with [[Client.connect]]
 * @extends NodeManager
 * @example
 * ```typescript
 * const client = new Client("system", "myapp")
 * await client.connect()
 * ```
 */
export class Client extends NodeManager {
    /**
     * Creates a new Client.
     * @param domain Application domain
     * @param appId Application identifier
     * @param options {NodeManagerOptions} Options
     */
    constructor(domain, appId: string, options: NodeManagerOptions = null) {
        super(new ExtendedNatsClient(domain, appId, options ? options.hubId : null), options);
    }

    /**
     * Try to connect this client to vBus.
     * @async
     */
    async connect() {
        await this.client.connect();
        await this.initialize();
    }

    /**
     * Disconnect this client.
     */
    close() {
        this.client.close()
    }

    /**
     * The current hostname.
     * @return {string} hostname
     */
    getHostname(): string {
        return this.client.getHostname()
    }

    /**
     * The app id.
     * The app id is equal to ${app_domain}.${app_name}
     * @return {string} app id
     */
    getId(): string {
        return this.client.getId()
    }

    /**
     * Request authorization for a vBus path.
     * It can contains "*" to indicate any segment or ">" to indicate any segment after.
     * @param permission A permission string, example: "system.zigbee.>"
     * @example
     * const ok = await client.askPermission("system.zigbee.>")
     */
    async askPermission(permission: string): Promise<boolean> {
        return await this.client.askPermission(permission)
    }
}
