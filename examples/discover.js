const vbus = require("../dist");


// set vbus library log level
vbus.setLoggerLevel("debug");


// an utility method to recursively traverse a vbus node
function traverseNode(node, level) {
    const elements = node.elements();
    for (let name in elements) {
        const e = elements[name];

        if (e.isNode()) {
            const n = e.asNode();
            console.log(`${" ".repeat(level * 2)}${name}`);
            traverseNode(n, level + 1);
        } else if (e.isAttribute()) {
            const attr = e.asAttribute();
            console.log(`${" ".repeat(level * 2)}${name} = ${attr.getValue()}`);
        } else if (e.isMethod()) {
            console.log(`${" ".repeat(level * 2)}${name}`);
        }
    }
}

// top level await (vbus library is async)
(async () => {
    try {
        const client = new vbus.Client("system", "testjs", {static_path: ""});
        await client.connect();
        console.log("connected");

        // add a node
        client.addNode("foo", {
            bar: {
                baz: {
                    name: new vbus.AttributeDef("name", "eliott", {type: "string"}),
                    scan: new vbus.MethodDef((args, path) => {
                        return new Promise(((resolve, reject) => {
                            console.log(args);
                            console.log(path);
                            resolve(42)
                        }))
                    }, {}, {})
                }
            }
        });

        // discover zigbee
        const ok = await client.askPermission("system.zigbee");

        const elem = await client.discover("system.zigbee", 1000);
        if (elem.isNode()) {
            traverseNode(elem.asNode(), 0)
        }
    } catch (e) {
        console.log(e)
    }
})();

